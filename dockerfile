FROM vital987/chrome-novnc:latest
RUN apk update && apk add socat
COPY 5-chromium.conf /config/supervisor/5-chromium.conf
COPY 7-socat.conf /config/supervisor/7-socat.conf
EXPOSE 9221
EXPOSE 9870
ENTRYPOINT supervisord -l /var/log/supervisord.log -c /config/supervisord.conf